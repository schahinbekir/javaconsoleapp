package reklamAjansı;

import java.util.ArrayList;

import reklamAjansı.oyuncular;

public class oyuncular extends calisanlar implements oyuncularInterface{
	static ArrayList<oyuncular> ArrayOyuncular=new ArrayList<oyuncular>();
	private String Kategori;

	public oyuncular(String Tc, String Ad, String Soyad, String IsDurumu, String BasTar, String BitTar, String Izin, int Maas, String Kategori) {
		super(Tc, Ad, Soyad, IsDurumu, BasTar, BitTar, Izin, Maas);
		this.Kategori=Kategori;
	}
	public String getKategori() {
		return Kategori;
	}
public void setKategori(String kategori) {
	Kategori = kategori;
}
static void addOyuncu(oyuncular oyuncuCalisan) {
	ArrayOyuncular.add(oyuncuCalisan);
}
String bilgileriGetir_Oyuncu(oyuncular oyuncuCalisan) {
	
	String oyuncuCalisanBilgi = "oyuncular\n" + "Sirasi: " + ArrayOyuncular.indexOf(oyuncuCalisan)
	+ "\n ID:" + oyuncuCalisan.getId() + "\n Tc:" + oyuncuCalisan.getTc() 
	+ "\n Ad:" + oyuncuCalisan.getAd() + "\n Soyad:" + oyuncuCalisan.getSoyad() 
	+ "\n Is Durumu:" + oyuncuCalisan.getIsDurumu() + "\n BasTar:" + oyuncuCalisan.getBasTar() 
	+ "\n BitTar:" + oyuncuCalisan.getBitTar() + "\nIzın" + oyuncuCalisan.getIzin() 
	+ "\nMaas:" + oyuncuCalisan.getMaas() + "\nKategori:" + oyuncuCalisan.getKategori();
	return oyuncuCalisanBilgi;
}
@Override
public void oyuncuEkle(oyuncular oyuncuCalisan) {
	ArrayOyuncular.add(oyuncuCalisan);
	System.out.println(oyuncuCalisan.getTc() + " Eklendi/Guncellendi.");
	
}
@Override
public void oyuncuSil(oyuncular oyuncuCalisan) {
	try {
		for (oyuncular artist:ArrayOyuncular) {
			if (oyuncuCalisan.getId()==artist.getId()) {
				ArrayOyuncular.remove(ArrayOyuncular.indexOf(oyuncuCalisan));
			}
		}
		System.out.println("Sildi");
		
	} catch (Exception e) {
		System.out.println("Sildi.");
	}
	
}
}
