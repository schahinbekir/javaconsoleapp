package reklamAjansı;

public class calisanlar {
	private static int IdC=0;
	private int Id;
	private String Tc;
	private String Ad;
	private String Soyad;
	private String IsDurumu;
	private String BasTar;
	private String BitTar;
	private String Izin;
	private int Maas;

public calisanlar(String Tc, String Ad, String Soyad, String IsDurumu, String BasTar, String BitTar, String Izin, int Maas) {
	
	this.Tc=Tc;
	this.Ad=Ad;
	this.Soyad=Soyad;
	this.IsDurumu=IsDurumu;
	this.BasTar=BasTar;
	this.BitTar=BitTar;
	this.Izin=Izin;
	this.Maas=Maas;
	IdC++;
	this.Id=IdC;
}
public int getId() {
	return Id;
}
public String getTc() {
	return Tc;
}
public void setTc(String tc) {
	Tc = tc;
}
public String getAd() {
	return Ad;
}
public void setAd(String ad) {
	Ad = ad;
}
public String getSoyad() {
	return Soyad;
}
public void setSoyad(String soyad) {
	Soyad = soyad;
}
public String getIsDurumu() {
	return IsDurumu;
}
public void setIsDurumu(String isDurumu) {
	IsDurumu = isDurumu;
}
public String getBasTar() {
	return BasTar;
}
public void setBasTar(String basTar) {
	BasTar = basTar;
}
public String getBitTar() {
	return BitTar;
}
public void setBitTar(String bitTar) {
	BitTar = bitTar;
}
public String getIzin() {
	return Izin;
}
public void setIzin(String izin) {
	Izin = izin;
}
public int getMaas() {
	return Maas;
}
public void setMaas(int maas) {
	Maas = maas;
}
public String BilgileriGetir(String CalisanBilgi) {
	return CalisanBilgi;
}
}